import { Component, OnInit } from '@angular/core';
import { Informacion } from 'src/app/interfaces/verificacion';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
    datos: Informacion = {
      correo:'',
      tramites:'2',
      descripcion:''
  }
  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{
    console.log("Mensaje Guardado");
    console.log(this.datos.correo);
    console.log(this.datos.tramites);
    console.log(this.datos.descripcion);
  }
}
